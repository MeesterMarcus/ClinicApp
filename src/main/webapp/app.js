/**
 * Created by Marcus on 6/30/2016.
 */
var app = angular.module('Clinic', ['ngAnimate', 'ui.router', 'ngMaterial', 'ngMdIcons', 'md.data.table']);

app.config(function ($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/home");
    //
    // Now set up the states
    $stateProvider
        .state('home', {
            url: "/home",
            templateUrl: "views/home.html"
        })
        .state('patients', {
            url: "/patients",
            controller: "patientController",
            controllerAs: "ctrl",
            templateUrl: "views/patientsTable.html"
        })
        .state('state2.list', {
            url: "/list",
            templateUrl: "partials/state2.list.html",
            controller: function ($scope) {
                $scope.things = ["A", "Set", "Of", "Things"];
            }
        });
});

app.controller('AppCtrl', ['$mdSidenav', function ($mdSidenav) {
    var vm = this;
    vm.toggleSidenav = function () {
        $mdSidenav().toggle();
    };
}])