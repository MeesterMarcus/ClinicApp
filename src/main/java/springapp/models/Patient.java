package springapp.models;

/**
 * Created by Marcus on 6/30/2016.
 */
public class Patient {

    private String lastName;
    private String firstName;
    private String city;
    private String state;
    private String zip;
    private String address;
    private String diagnosis;

    public Patient(String lastName, String firstName, String city, String state, String zip, String address) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.address = address;
        this.diagnosis = "None";
    }

    public Patient(String lastName, String firstName, String city, String state, String zip, String address,String diagnosis) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.address = address;
        this.diagnosis = diagnosis;
    }

    public Patient(){

    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
}
