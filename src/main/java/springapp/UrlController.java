package springapp;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springapp.models.Patient;

import java.util.ArrayList;

@RestController
public class UrlController {

    @RequestMapping("/patient")
    public Patient getPatient(){
        Patient patient = new Patient("Smith","John","San Antonio", "TX", "78232", "123 Address Ln");
        return patient;
    }

    @RequestMapping("/patient/{id}")
    public Patient getPatientById(@PathVariable("id") int id){
        return new Patient();
    }

    @RequestMapping("/getPatients")
    public ArrayList<Patient> getPatients(){
        Patient patient = new Patient("Smith","John","San Antonio", "TX", "78232", "123 Address Ln");
        Patient patient2 = new Patient("Oliver","John","Houston", "TX", "78232", "123 Address Ln");
        ArrayList<Patient> patientList = new ArrayList<Patient>();
        patientList.add(patient);
        patientList.add(patient2);
        return patientList;
    }
    
}
